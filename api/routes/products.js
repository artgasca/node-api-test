const express = require('express');
const router = express.Router();

router.get('/',(req,res,next)=>{
    res.status(200).json({
        message: "Get to products"
    });
});

router.post('/',(req,res,next)=>{
    res.status(200).json({
        message: "post to products"
    });
});

router.get('/:productId',(req,res,next)=>{
    const id = req.params.productId;
    if (id === 'special') {
        res.status(200).json({
            message : 'You discoverd the special ID'
        });

        
    }else{
        res.status(200).json({
            message : id
        })
    }
});

module.exports = router;